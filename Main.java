public class Main {
    public static void main(String[] args) {
        MyLinkedList<Integer> myLinked=new MyLinkedList<>();
        myLinked.addTail(34);
        myLinked.addTail(56);
        myLinked.addTail(23);
        myLinked.addTail(34);
        myLinked.addTail(98);
        myLinked.addTail(87);
        myLinked.addTail(67);
        myLinked.addHead(11);
        myLinked.addTail(22);

        System.out.println(myLinked.size());
        System.out.println(myLinked.get(0));
        System.out.println(myLinked);

        System.out.println(myLinked.getFirst());
        System.out.println(myLinked.getLast());

        System.out.println(myLinked);

        System.out.println(myLinked.indexOf(56));
        System.out.println(myLinked.contains(67));


    }
}
