import java.util.*;

public class MyLinkedList<T> implements List<T>
{
    private int size;
    private Node<T> first;
    private Node<T> last;

    public void addTail(T tail) {
        Node<T> newNode = new Node<>(tail);

        if (last == null) {
            first = newNode;
            last = newNode;
        } else {
            newNode.setPrev(last);
            last.setNext(newNode);
            last = newNode;
        }
        size++;
    }

    public void addHead(T head) {
        Node<T> newNode = new Node<>(head);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            newNode.setNext(first);
            first.setPrev(newNode);
            first = newNode;
        }
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if(this.first==null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> current=first;
        while(current!=null){
            if(current.getData().equals(o)){
                return true;
            }
            current=current.getNext();
        }
        return false;
    }
    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        Object [] arr=new Object[size];
        Node<T> current=first;
        for(int i=0;i<size;i++){
            arr[i]=current.getData();
            current=current.getNext();
        }
        return arr;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> removed=first;
        while(removed!=null){
            if(removed.getData().equals(o)){
                Node<T> prev=removed.getPrev();
                Node<T> next=removed.getNext();
                if(prev!=null || next!=null){
                    prev.setNext(next);
                    next.setPrev(prev);
                }
                else{
                    first=next;
                    last=prev;
                }
                size--;
                return true;
            }
            removed=removed.getNext();
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    public T getFirst() {
        if (first == null) {
            throw new NoSuchElementException("List is empty");
        }
        return first.getData();
    }

    public T getLast() {
        if (last == null) {
            throw new NoSuchElementException("List is empty");
        }
        return last.getData();
    }

    @Override
    public T get(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }
        Node<T> current = first;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current.getData();
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    public T remove(int index){
        if(index<0 || size<=index){
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        Node<T> current=first;
        for(int i=0;i<index;i++){
            current=current.getNext();
        }

        if(current==first && current==last){
            first=null;
            last=null;
        }
        else if(current==first){
            first=first.getNext();
            first.setPrev(null);
        }
        else if(current==last){
            last=last.getPrev();
            last.setNext(null);
        }
        else{
           Node<T> removeNode= (Node<T>) get(index);
           Node<T> tempPrev= removeNode.getPrev();
           Node<T> tempNext= removeNode.getNext();
           tempPrev.setNext(tempNext);
           tempNext.setPrev(tempPrev);
           return removeNode.getData();
        }

        T removed=current.getData();
        size--;
        return removed;
    }

    @Override
    public int indexOf(Object o) {
        Node<T> current=first;
        int index=0;

        while (!current.getData().equals(o)){
            current=current.getNext();
            index++;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        Node<T> current = first;
        while (current != null) {
            sb.append(current.getData());
            if (current.getNext() != null) {
                sb.append(", ");
            }
            current = current.getNext();
        }

        return sb.toString();
    }


}
